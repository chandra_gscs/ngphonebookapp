angular.module('phoneBookApp')
    .controller('AppCtrl', ['$scope', '$mdSidenav', '$mdDialog', function ($scope, $mdSidenav, $mdDialog) {
        $scope.data = [{
            "name": "sekhar",
            "no": "0970-3297446",
            "email": "sekhargscs@gmail.com"
							}, {
            "name": "chandra",
            "no": "0970-3290000",
            "email": "sekhar@gmail.com"
							}, {
            "name": "sekhar",
            "no": "0970-3000046",
            "email": "gscs@gmail.com"
							}];


        $scope.addItem = function (event) {
            actionPerformed(event, {}, "add");
        }

        $scope.viewItem = function (itemArg, event) {
            actionPerformed(event, itemArg, "view");
        }
        
        $scope.editItem = function (itemArg, event) {
            actionPerformed(event, itemArg, "edit");
        }

        var actionPerformed = function (ev, contact, action) {
            $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'components/layout/views/addOrEditOrView.html',
                    targetEvent: ev,
                    locals: {
                        contact: contact,
                        action: action
                    }
                })
                .then(
                    function (contactMod) {
                        if (action == "edit") {
                            var insertAt = $scope.data.indexOf(contact);
                            $scope.data[insertAt] =contactMod;
                        } else if (action == "add") {
                            $scope.data.push(contactMod);
                        }
                    },
                    function () {
                        console.log('You cancelled the action.');
                    });
        };
        $scope.showConfirm = function (event, item) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete the record?')
                .textContent('Record will be deleted permanently.')
                .ariaLabel('TutorialsPoint.com')
                .targetEvent(event)
                .ok('Yes')
                .cancel('No');
            $mdDialog.show(confirm).then(function () {
                $scope.data.splice($scope.data.indexOf(item), 1);
                console.log('Record deleted successfully!')
            }, function () {
                console.log('You decided to keep your record.');
            });
        };
    }]);

function DialogController($scope, $mdDialog, contact, action) {
    var personCopy = angular.copy(contact);
    $scope.readOnly = action != "edit" && action != "add";
    $scope.person = personCopy ? personCopy : {};
    $scope.regExForEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
    $scope.regExForMobile = /^\d{4}-\d{7}$/;
    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.save = function (userForm) {
        if (userForm.$valid) {
            $mdDialog.hide($scope.person);
        }
    };
};