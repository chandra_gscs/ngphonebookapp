angular.module('phoneBookApp', ['ui.router', 'ngAria',
    'ngMaterial', 'ngMessages', 'ngAnimate'
])

.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider

            .state('home', {
            url: '/',
            controller:'AppCtrl',
            templateUrl: 'components/layout/contacts.template.html'
        });

    }
]);
